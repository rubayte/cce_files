Repo for additional files for the cce pipeline

1. phiX174.fa
==============
contains the fasta seq for Enterobacteria phage phiX174 sensu lato, complete genome
(https://www.ncbi.nlm.nih.gov/nuccore/NC_001422.1?report=fasta)

2. CMDL_130217_capture_targets_noChr.bed
=========================================
target bed file

3. MVL1.sorted.vcf
==================
NKI MVL snps
version: 2020_02_20

4. MVL1.rest.tsv
================
NKI MVL indels
version: 2020_02_20


5. MVL2.sorted.vcf
==================
OncoKB snps
version: 20190114

6. MVL2.rest.tsv
================
OncoKB indels
version: 20190114

